<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {

        try {
            $data = $request->all();
            $category = Category::create($data);

            return redirect()->route('categories.index')->withMessage('Category Inserted Successfully.');
        } catch (QueryException $e) {
            return redirect()
                ->route('categories.create')
                ->withInput()
                ->withErrors($e->getMessage());

        }

    }

    public function show(Category $category)
    {
        return view('categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        try {
            $data = $request->all();
            $category->update($data);
            return redirect()->route('categories.index')->withMessage('Category Updated Successfully.');
        } catch (QueryException $e) {
            return redirect()
                ->route('categories.create')
                ->withInput()
                ->withErrors($e->getMessage());

        }
    }

    public function destroy(Category $category)
    {
        try {
            $category->delete();
            return redirect()
                ->route('categories.index')
                ->withMessage('Category Deleted Successfully!');

        } catch (QueryException $e) {
            return redirect()
                ->route('categories.index')
                ->withInput()
                ->withErrors($e->getMessage());
        }
    }


}
