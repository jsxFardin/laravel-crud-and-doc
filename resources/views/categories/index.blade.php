<?php
/**
 * Created by PhpStorm.
 * User: fssha
 * Date: 6/11/2019
 * Time: 4:17 PM
 */
?>
<table>
   <a href="{{route('categories.create')}}">Add Category</a>
   <tr>
       <th>Sl</th>
       <th>Title</th>
       <th>Action</th>
   </tr>
@foreach($categories as $category)
    <tr>
        <td>{{$category->id}}</td>
        <td>{{$category->title}}</td>
        <td>
            <a href="{{route('categories.edit',$category->id)}}" >Edit</a>
            <a  href="{{route('categories.show',$category->id)}}">Show</a>
            <form action="{{route('categories.destroy',$category->id)}}" method="post" style="display: inline-block">
                @csrf
                @method('delete')
                <button type="submit">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
    </table>
