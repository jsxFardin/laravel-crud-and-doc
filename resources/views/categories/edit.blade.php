
{{ Form::model($category,[
       'route'=>['categories.update', $category->id],
       'method'=>'put'
       ])}}

@include('categories.form')

{{ Form::close() }}