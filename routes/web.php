<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $obj = \App\Category::all();
    Debugbar::info($obj);
    return view('welcome');
});

Route::resource('categories', 'CategoryController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'articles',
], function () {
    Route::get('/', 'ArticlesController@index')
         ->name('articles.article.index');
    Route::get('/create','ArticlesController@create')
         ->name('articles.article.create');
    Route::get('/show/{article}','ArticlesController@show')
         ->name('articles.article.show');
    Route::get('/{article}/edit','ArticlesController@edit')
         ->name('articles.article.edit');
    Route::post('/', 'ArticlesController@store')
         ->name('articles.article.store');
    Route::put('article/{article}', 'ArticlesController@update')
         ->name('articles.article.update');
    Route::delete('/article/{article}','ArticlesController@destroy')
         ->name('articles.article.destroy');
});


